import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MonitorModule } from './modules/monitor/monitor.module';
import { AppRoutingModule } from './app-routing.module';
import { TableModule } from 'primeng/table';
import { ComponentsModule } from './components/components.module';
import { KeycloakService, KeycloakAngularModule } from 'keycloak-angular';
import { initializerKeycloak } from './keycloak-init';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { UserModule } from './modules/user/user.module';
import { EnterpriseComponent } from './enterprise/enterprise.component';
import {DialogModule} from 'primeng/dialog';
import {DropdownModule} from 'primeng/dropdown';
import {ButtonModule} from 'primeng/button';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EnterpriseComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ComponentsModule,
    MonitorModule,
    UserModule,
    TableModule,
    DialogModule,
    DropdownModule,
    ButtonModule,
    KeycloakAngularModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializerKeycloak,
      multi: true,
      deps: [KeycloakService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
