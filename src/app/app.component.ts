import { Component, OnInit } from '@angular/core';
import { MainGuard } from './services/main.guard';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(public mainGuard: MainGuard) {
    console.log('=== app component ===');
    // this.userType = this.userService.loggedUser.profile['userType'];
  }

  ngOnInit() {
  }

}
