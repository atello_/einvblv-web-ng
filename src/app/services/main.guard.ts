import { KeycloakService } from 'keycloak-angular';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserService } from './user.service';

@Injectable({
    providedIn: 'root'
})
export class MainGuard implements CanActivate {
    canActivateFlag: boolean;
    loggedIn = false;

    constructor(protected router: Router, private keycloakService: KeycloakService, private userService: UserService) {
    }

    canActivate(): boolean {
        console.log('=== MainGuard activated === ');
        // return new Promise((resolve, reject) => {
        // if (!this.authenticated) {
        //     console.log('=== NOT AUTHORIZED === ');
        //     this.keycloakAngular.login()
        //         .catch(e => console.error(e));
        //     return reject(false);
        // }+
        this.keycloakService.isLoggedIn().then(isLogged => {
            if (isLogged) {
                this.keycloakService.loadUserProfile().then(userInfo => {
                    console.log('=== setting user data from keycloak === ');
                    this.setUser(userInfo);
                    this.canActivateFlag = true;
                    this.loggedIn = true;
                });
            } else {
                console.log('NOT LOGGED IN');
                this.canActivateFlag = false;
                this.loggedIn = false;
                this.keycloakService.login().catch((e) => console.error(e));
            }
        });
        return this.canActivateFlag;
    }

    /**
     * 
     * @param userInfo
     */
    setUser(userInfo: any) {
        this.userService.loggedUser.email = userInfo.email;
        this.userService.loggedUser.firstName = userInfo.firstName;
        this.userService.loggedUser.lastName = userInfo.lastName;
        this.userService.loggedUser.username = userInfo.username;
        this.userService.loggedUser.profile = JSON.parse(userInfo['attributes'].profileId[0]);
        console.log('user info', this.userService.loggedUser);
    }

}
