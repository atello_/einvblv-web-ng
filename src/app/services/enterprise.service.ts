import { Injectable } from '@angular/core';
import {MainService} from './main.service';
import {UserModel} from '../models/user.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EnterpriseService extends MainService {

  loggedUser: UserModel;

  routeUrl = 'enterprise/';
  constructor(httpCliente: HttpClient) {
    super(httpCliente);
    this.loggedUser = new UserModel();
  }

  getEnterPrises() {
    return this.get(`${this.routeUrl + 'getEnterpriseList'}`);
  }
}
