import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel } from '../models/user.model';
import { MainService } from './main.service';

@Injectable({ providedIn: 'root' })
export class UserService extends MainService {
    loggedUser: UserModel;

    constructor(httpCliente: HttpClient) {
        super(httpCliente);
        this.baseUrl = 'assets/';
        this.loggedUser = new UserModel();
    }

    /**
     * Return menu data by user
     */
    getMenu(user: any) {
        return this.get('menu.json');
    }

}
