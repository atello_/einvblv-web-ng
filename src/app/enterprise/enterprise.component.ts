import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { EnterpriseService } from '../services/enterprise.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-enterprise',
  templateUrl: './enterprise.component.html',
  styleUrls: ['./enterprise.component.css']
})
export class EnterpriseComponent implements OnInit {

  enterprises: any[] = [];
  selectedEnterprise: any;
  userType = '';
  displaySelectEnterprise = false;
  displayHome = false;

  constructor(private userService: UserService,
    private enterprisesService: EnterpriseService,
    private router: Router) {

    console.log('== enterprise ===');
    this.userType = this.userService.loggedUser.profile['userType'];
  }

  ngOnInit() {
    if (this.userType === '1') {
      this.getEnterprises();
    }
  }


  getEnterprises() {
    this.enterprisesService.getEnterPrises().subscribe(response => {
      this.enterprises = response.data;
      this.displaySelectEnterprise = true;
    });
  }

  redirectToMenu() {
    this.userService.loggedUser.selectedEnterprise = this.selectedEnterprise.enterpriseid;
    this.displaySelectEnterprise = false;
    this.displayHome = true;
    // this.router.navigate(["/home"]);
  }


}
