import { KeycloakConfig } from 'keycloak-angular';

export const keycloakConfiguration: KeycloakConfig = {
    url: 'https://192.168.5.243:8443/auth',
    realm: 'einvblv',
    clientId: 'einvblv-id',
    credentials: {
        secret: 'f74986c1-19fb-4f39-a5f2-dfef3e7e6270'
    }
};
