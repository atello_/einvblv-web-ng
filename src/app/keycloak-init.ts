import { KeycloakService } from 'keycloak-angular';
import { keycloakConfiguration } from './keycloak-conf';


export function initializerKeycloak(keycloakService: KeycloakService): () => Promise<any> {
    return (): Promise<any> => {
        return new Promise(async (resolve, reject) => {
            console.log('KEY CLOAK INIT');
            try {
                // await keycloak.init();
                await keycloakService.init({
                    config: keycloakConfiguration,
                    initOptions: {
                        onLoad: 'login-required',
                        checkLoginIframe: false
                    },
                    enableBearerInterceptor: true,
                    loadUserProfileAtStartUp: true,
                    bearerExcludedUrls: [
                        '/assets',
                        '/clients/public'
                    ],
                }).then(() => {
                    resolve();
                });
            } catch (error) {
                reject(error);
            }
        });
    };
}
