import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainService } from '../../../services/main.service';
import { monitorConst } from '../constants/monitor-constant';

/**
 * @author atello-barcia
 */
@Injectable()
export class MonitorService extends MainService {

    routeUrl = 'monitor/';
    constructor(protected http: HttpClient) {
        super(http);
    }

    /**
     * Returns all monitor data
     */
    getMonitor() {
        return this.get(`${this.routeUrl + monitorConst.WS.GET_MONITOR}`);
    }

}
