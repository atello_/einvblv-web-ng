import { Component, OnInit } from '@angular/core';
import { MonitorService } from '../../services/monitor.service';

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss']
})
export class InvoiceListComponent implements OnInit {

  monitor: any[];

  constructor(private monitorService: MonitorService) {
    console.log('=== monitor ===');
  }

  ngOnInit() {
    this.getMonitor();
  }

  getMonitor() {
    this.monitorService.getMonitor().subscribe(response => this.monitor = response.data);
  }

}
