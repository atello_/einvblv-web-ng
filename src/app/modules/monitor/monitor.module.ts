import { NgModule } from '@angular/core';

// Modules
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { MonitorRoutingModule } from './monitor-routing.module';
import { ComponentsModule } from '../../components/components.module';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';


// Services
import { MonitorService } from './services/monitor.service';

// Components
import { InvoiceListComponent } from './components/invoice-list/invoice-list.component';


@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MonitorRoutingModule,
    ComponentsModule,
    PanelModule,
    TableModule,
  ],
  declarations: [InvoiceListComponent],
  providers: [MonitorService]
})
export class MonitorModule { }
