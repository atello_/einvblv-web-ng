import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoiceListComponent } from './components/invoice-list/invoice-list.component';

const routes: Routes = [
    { path: 'list-invoices', component: InvoiceListComponent, outlet: 'sidebaroutlet' }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MonitorRoutingModule { }
