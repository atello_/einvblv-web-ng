import { Injectable } from '@angular/core';
import {MainService} from '../../../services/main.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService extends MainService{

  constructor(httpCliente: HttpClient) {
    super(httpCliente);
    this.baseUrl = 'assets/';
  }

  getUserList(){
    return this.get('users.json');
  }
}
