import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {UserInfo} from '../../model/UserInfo';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  userList: UserInfo[];
  newUser:UserInfo = new UserInfo();
  modalUserDisplay: boolean = false;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUserList();
  }
  
  showModalUser(){
    this.modalUserDisplay= true;
  }

  getUserList(){
    this.userService.getUserList().subscribe(response =>{
      this.userList = response;
    });
  }

  addNewUser(){
    this.modalUserDisplay = false;
    this.userList.push(this.newUser);
  }

}
