import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreateUserComponent} from './components/create-user/create-user.component';

const routes: Routes = [
  { path: 'create-user', component: CreateUserComponent, outlet: 'sidebaroutlet' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
