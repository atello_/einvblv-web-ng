import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { CreateUserComponent } from './components/create-user/create-user.component';
import {PanelModule} from 'primeng/panel';
import { TableModule } from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {DialogModule} from 'primeng/dialog';
import {InputTextModule} from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    PanelModule,
    ButtonModule,
    TableModule,
    InputTextModule,
    FormsModule,
    DialogModule
  ],
  declarations: [CreateUserComponent]
})
export class UserModule { }
