/**
 * @author atello-barcia
 */
export class UserModel {
    public email: string;
    public id: string;
    public firstName: string;
    public lastName: string;
    public username: string;
    public profile: string;
    public selectedEnterprise?:number;
}
