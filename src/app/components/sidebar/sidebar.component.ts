import { Component, OnInit, Input } from '@angular/core';
import { NavItem } from '../nav-item.interface';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() menu: NavItem[];

  constructor() {
    console.log('<== sidebar template ==>');
  }

  ngOnInit() {
  }

}
