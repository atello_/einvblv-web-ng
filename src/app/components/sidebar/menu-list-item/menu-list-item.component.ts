import { Component, OnInit, Input } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NavItem } from '../../nav-item.interface';
import { Router, RouterState } from '@angular/router';

@Component({
  selector: 'app-menu-list-item',
  templateUrl: './menu-list-item.component.html',
  styleUrls: ['./menu-list-item.component.scss'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition('expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ])
  ]
})
export class MenuListItemComponent implements OnInit {

  @Input() item: NavItem;
  @Input() depth: number;

  constructor(private router: Router) {
    if (this.depth === undefined) {
      this.depth = 0;
    }
  }

  ngOnInit() { }

  /**
   * On item selected
   * @param item
   */
  onItemSelected(item: NavItem) {
    if (!item.children || !item.children.length) {
      this.router.navigate([{
        outlets: {
          sidebaroutlet: [item.route]
        }
      }]);
    }
    if (item.children && item.children.length) {
      item.expanded = !item.expanded;
    }
  }

}
