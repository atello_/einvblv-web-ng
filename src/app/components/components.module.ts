import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PanelMenuModule } from 'primeng/panelmenu';
import {
    MatToolbarModule, MatSidenavModule, MatButtonModule,
    MatListModule, MatIconModule, MatMenuModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { MenuListItemComponent } from './sidebar/menu-list-item/menu-list-item.component';

/**
 * @author atello-barcia
 */
@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        FormsModule,
        PanelMenuModule,
        MatToolbarModule,
        MatSidenavModule,
        MatButtonModule,
        MatListModule,
        MatMenuModule,
        MatIconModule,
        FlexLayoutModule,
    ],
    declarations: [MainLayoutComponent, SidebarComponent, NavigationBarComponent, MenuListItemComponent],
    exports: [MainLayoutComponent, SidebarComponent]

})
export class ComponentsModule { }
