import { Component, OnInit, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  @Input() sidebar: TemplateRef<any>;
  @Input() header: TemplateRef<any>;
  opened = true;

  constructor() {
    console.log('=== main layout init ===');
  }

  ngOnInit() {
  }

}
