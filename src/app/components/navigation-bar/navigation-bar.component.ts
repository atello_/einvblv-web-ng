import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';


@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss']
})
export class NavigationBarComponent implements OnInit {

  @Output() toggleSidenav = new EventEmitter<void>();
  @Input() enterpriseName: string;

  constructor(private keycloakService: KeycloakService) { }

  ngOnInit() {
  }

  /**
   * Log out
   */
  logout() {
    this.keycloakService.logout();
  }

}
