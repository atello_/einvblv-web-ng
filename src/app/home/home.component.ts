import { Component, OnInit } from '@angular/core';
import { NavItem } from '../components/nav-item.interface';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  userMenu: NavItem[] = [];

  constructor(private userService: UserService) {
    console.log('=== home component ===');
  }

  ngOnInit() {
    this.getMenuByUser();
    console.log("Enterpise selected:", this.userService.loggedUser.selectedEnterprise);
  }

  /**
  * Get menu data by user
  */
  getMenuByUser() {
    this.userService.getMenu(this.userService.loggedUser).subscribe(response => this.userMenu = response.data);
  }
}
